package ir.snappbox.phonebook.contacts.dto;

import io.swagger.annotations.ApiModelProperty;
import ir.snappbox.phonebook.util.validation.PhoneNumber;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContactDto {

    @ApiModelProperty(
            value = "name of the contact",
            name = "name",
            dataType = "String",
            example = "alireza")
    @Size(min = 2,max=52)
    private String name;


    @ApiModelProperty(
            value = "phone number of the contact according to the current country that is extracted from accept-language header ",
            name = "phone number",
            dataType = "String",
            example = "09371085854")
    @Size(min = 2,max=26)
    @PhoneNumber
    private String phoneNumber;

    @ApiModelProperty(
            value = "email of the contact",
            name = "email",
            dataType = "String",
            example = "alireza.hanifi@gmail.com")
    @Size(min = 2,max=62)
    @Email
    private String email;

    @ApiModelProperty(
            value = "name of the contact's organization",
            name = "organization",
            dataType = "String",
            example = "snappbox")
    @Size(min = 2,max=50)
    private String organization;

    @ApiModelProperty(
            value = "id of the contact's github",
            name = "github id",
            dataType = "String",
            example = "alireza-hanifi")
    @Size(min = 2,max=64)
    private String github;

}
