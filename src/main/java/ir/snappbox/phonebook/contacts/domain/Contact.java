package ir.snappbox.phonebook.contacts.domain;

import com.querydsl.core.annotations.QueryEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@QueryEntity
@Document
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Contact {

    private @Id String id;

    @Size(min = 2,max=52)
    @Indexed
    private @Field String name;

    @Size(min = 2,max=26)
    @Indexed
    private @Field String phoneNumber;

    @Size(min = 2,max=62)
    @Indexed
    private @Field String email;

    @Size(min = 2,max=50)
    @Indexed
    private @Field String organization;

    @Size(min = 2,max=64)
    @Indexed
    private @Field String github;

    private Set<GithubRepository> githubRepositories=new HashSet<>();
}
