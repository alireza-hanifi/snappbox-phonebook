package ir.snappbox.phonebook.contacts.domain;

import lombok.Data;

@Data
public class GithubRepository {
    private String name;
}
