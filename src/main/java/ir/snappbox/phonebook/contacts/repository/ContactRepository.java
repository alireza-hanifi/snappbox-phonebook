package ir.snappbox.phonebook.contacts.repository;

import ir.snappbox.phonebook.contacts.domain.Contact;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ContactRepository extends MongoRepository<Contact, String>, QuerydslPredicateExecutor<Contact> {

}
