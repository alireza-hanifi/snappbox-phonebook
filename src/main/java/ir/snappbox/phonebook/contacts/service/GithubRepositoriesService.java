package ir.snappbox.phonebook.contacts.service;

import ir.snappbox.phonebook.config.GithubConfiguration;
import ir.snappbox.phonebook.contacts.domain.GithubRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

@Service
@Log4j2
@RequiredArgsConstructor
public class GithubRepositoriesService {

    private final GithubConfiguration githubConfiguration;

    public Flux<ResponseEntity<Set<GithubRepository>>> fetchGithubRepositories(String githubId) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(githubConfiguration.getGithubApiAddress() + "/users/{githubId}/repos");
        Map<String, String> urlVars = new HashMap<>();
        urlVars.put("githubId", githubId);
        var baseUri = uriBuilder.buildAndExpand(urlVars).toUriString();
        return WebClient.create()
                .get()
                .uri(baseUri)
                .retrieve()
                .toEntity(new ParameterizedTypeReference<Set<GithubRepository>>() {
                })
                .timeout(Duration.ofMillis(Integer.parseInt(githubConfiguration.getRequestTimeout())))
                .retryWhen(Retry.backoff(Integer.parseInt(githubConfiguration.getMaxRetry()), Duration.ofSeconds(Integer.parseInt(githubConfiguration.getRetryDelayInSecond())))
                        .doAfterRetry(retrySignal -> log.warn(baseUri + retrySignal))
                        .onRetryExhaustedThrow((retryBackoffSpec, retrySignal)
                                -> new TimeoutException()))
                .expand(clientResponse ->
                        clientResponse.getHeaders().getValuesAsList("LINK")
                                .stream()
                                .filter(link -> link.matches("<([^>]*)>;\\s*rel=\"([^\"]*)\""))
                                .map(link -> Pattern.compile("<([^>]*)>;\\s*rel=\"([^\"]*)\"").matcher(link).results().toArray())
                                .filter(matcher -> ((MatchResult) matcher[0]).group(2).equals("next"))
                                .map(matcher -> WebClient.create().get()
                                        .uri(((MatchResult) matcher[0]).group(1))
                                        .retrieve()
                                        .toEntity(new ParameterizedTypeReference<Set<GithubRepository>>() {
                                        })
                                        .timeout(Duration.ofMillis(10_000))
                                        .retryWhen(Retry.backoff(10, Duration.ofSeconds(5))
                                                .doAfterRetry(retrySignal -> log.warn("retry "+ baseUri + " " + retrySignal.totalRetries()))
                                                .onRetryExhaustedThrow((retryBackoffSpec, retrySignal)
                                                        -> new TimeoutException())))
                                .findFirst()

                                .orElseGet(Mono::empty));
    }


}
