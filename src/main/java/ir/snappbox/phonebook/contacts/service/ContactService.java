package ir.snappbox.phonebook.contacts.service;

import com.google.i18n.phonenumbers.NumberParseException;
import com.querydsl.core.BooleanBuilder;
import ir.snappbox.phonebook.contacts.domain.GithubRepository;
import ir.snappbox.phonebook.contacts.domain.QContact;
import ir.snappbox.phonebook.contacts.dto.ContactDto;
import ir.snappbox.phonebook.contacts.mapper.ContactMapper;
import ir.snappbox.phonebook.contacts.repository.ContactRepository;
import ir.snappbox.phonebook.util.PhoneNumberUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Set;

@Service
@RequiredArgsConstructor
@Log4j2
public class ContactService {
    private final ContactMapper contactMapper;
    private final ContactRepository contactRepository;
    private final GithubRepositoriesService githubRepositoriesService;

    public void addNewContact(ContactDto contactDto) {
        var contact = contactMapper.contactDtoToContact(contactDto);
        contact = contactRepository.save(contact);
        if (StringUtils.hasText(contact.getGithub())){
            var finalContact = contact;
            githubRepositoriesService.fetchGithubRepositories(contact.getGithub()).
            subscribe(repo -> addGithubRepositories(finalContact.getId(), repo.getBody()));
        }

    }

    public Iterable<ContactDto> searchContacts(ContactDto contactDto, Pageable pageable) throws NumberParseException {
        BooleanBuilder predicate = new BooleanBuilder();
        if (StringUtils.hasText(contactDto.getName())) {
            predicate.and(QContact.contact.name.contains(contactDto.getName()));
        }
        if (StringUtils.hasText(contactDto.getEmail())) {
            predicate.and(QContact.contact.email.contains(contactDto.getEmail()));
        }
        if (StringUtils.hasText(contactDto.getGithub())) {
            predicate.and(QContact.contact.github.contains(contactDto.getGithub()));
        }
        if (StringUtils.hasText(contactDto.getOrganization())) {
            predicate.and(QContact.contact.organization.contains(contactDto.getOrganization()));
        }
        if (StringUtils.hasText(contactDto.getPhoneNumber())) {
            predicate.and(QContact.contact.phoneNumber.contains(PhoneNumberUtils.mapToE164(contactDto.getPhoneNumber())));
        }
        if (pageable.isPaged())
            return contactMapper.contactIterableToContactDtoIterable(contactRepository.findAll(predicate, pageable));
        return contactMapper.contactIterableToContactDtoIterable(contactRepository.findAll(predicate));
    }

    private void addGithubRepositories(String contactId, Set<GithubRepository> githubRepositories) {
        var contact = contactRepository.findById(contactId);
        contact.ifPresent(c -> {
            c.getGithubRepositories().addAll(githubRepositories);
            contactRepository.save(c);
        });
    }



}
