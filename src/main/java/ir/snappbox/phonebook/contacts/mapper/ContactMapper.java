package ir.snappbox.phonebook.contacts.mapper;

import ir.snappbox.phonebook.config.MapstructConfig;
import ir.snappbox.phonebook.contacts.domain.Contact;
import ir.snappbox.phonebook.contacts.dto.ContactDto;
import ir.snappbox.phonebook.util.PhoneNumberMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapstructConfig.class, componentModel = "spring")
public interface ContactMapper {

    @Mapping(source = "phoneNumber",target = "phoneNumber",qualifiedBy = PhoneNumberMapping.class)
    Contact contactDtoToContact(ContactDto contactDto);

    ContactDto contactToContactDto(Contact contact);

    Iterable<ContactDto> contactIterableToContactDtoIterable(Iterable<Contact> contacts);

}
