package ir.snappbox.phonebook.util.validation;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneNumberValidator implements
        ConstraintValidator<PhoneNumber, String> {
 

    @Override
    public boolean isValid(String field, ConstraintValidatorContext cxt) {
        try {
            if (!StringUtils.hasText(field))
                return true;
            Phonenumber.PhoneNumber phoneNumber = PhoneNumberUtil.getInstance().parse(field, LocaleContextHolder.getLocale().getCountry());
            return PhoneNumberUtil.getInstance().isValidNumber(phoneNumber);
        } catch (NumberParseException e) {
            return false;
        }
    }
 
}