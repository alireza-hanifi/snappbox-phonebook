package ir.snappbox.phonebook.util;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.util.StringUtils;

public class PhoneNumberUtils {

    private PhoneNumberUtils(){}

    @PhoneNumberMapping
    public static String mapToE164(String rawPhoneNumber) throws NumberParseException {
        if (!StringUtils.hasText(rawPhoneNumber))
            return null;
        Phonenumber.PhoneNumber phoneNumber = PhoneNumberUtil.getInstance().parse(rawPhoneNumber, LocaleContextHolder.getLocale().getCountry());
        return PhoneNumberUtil.getInstance().format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
    }


}