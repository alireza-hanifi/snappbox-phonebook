package ir.snappbox.phonebook.rest;

import com.google.i18n.phonenumbers.NumberParseException;
import io.swagger.annotations.*;
import ir.snappbox.phonebook.contacts.dto.ContactDto;
import ir.snappbox.phonebook.contacts.service.ContactService;
import ir.snappbox.phonebook.util.ApiPageable;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Api
public class ContactController {

    private final ContactService contactService;

    @ApiOperation(value = "add new contact")
    @PutMapping("/contacts")
    public void addNewContact(@RequestBody @Valid ContactDto contactDto){
        contactService.addNewContact(contactDto);
    }

    @ApiOperation(value = "search contacts")
    @PostMapping("/contacts/search")
    @ApiPageable
    public Iterable<ContactDto> searchContacts(@RequestBody @Valid ContactDto contactDto,@ApiIgnore(
            "Ignored because swagger ui shows the wrong params, " +
                    "instead they are explained in the implicit params"
    ) Pageable pageable) throws NumberParseException {
        return contactService.searchContacts(contactDto,pageable);
    }
}
