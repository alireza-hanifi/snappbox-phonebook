package ir.snappbox.phonebook.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component("applicationProperties")
@ConfigurationProperties(prefix = "application")
@EnableConfigurationProperties
@Data
public class GithubConfiguration {
    private String githubApiAddress;
    private String maxRetry;
    private String requestTimeout;
    private String retryDelayInSecond;
}
