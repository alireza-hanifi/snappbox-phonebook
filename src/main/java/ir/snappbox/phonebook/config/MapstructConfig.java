package ir.snappbox.phonebook.config;

import ir.snappbox.phonebook.util.PhoneNumberUtils;
import org.mapstruct.MapperConfig;

@MapperConfig(uses = { PhoneNumberUtils.class})
public interface MapstructConfig {

}