package ir.snappbox.phonebook.it;

import com.google.i18n.phonenumbers.NumberParseException;
import ir.snappbox.phonebook.contacts.dto.ContactDto;
import ir.snappbox.phonebook.contacts.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.junit.Assert;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Collection;
import java.util.Locale;


@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Category(Integration.class)
@RequiredArgsConstructor
@TestPropertySource(locations = "/application.yml")
class ContactServiceTest {

    @BeforeEach
    void setUp() {
        LocaleContextHolder.setDefaultLocale(new Locale("fa", "IR"));
    }

    @Autowired
    private ContactService contactService;

    @Container
    public static MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:latest");

    @DynamicPropertySource
    static void mongoDbProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    @Test
    void searchTest() throws NumberParseException {
        contactService.addNewContact(ContactDto.builder()
                .name("alireza hanifi")
                .email("alireza.hanifi@gmail.com")
                .phoneNumber("09371085854")
                .organization("snappbox")
                .github("alireza-hanifi")
                .build()
        );
        var contactDtos = contactService.searchContacts(ContactDto.builder()
				.name("alireza hanifi")
                .email("alireza.hanifi@gmail.com")
                .phoneNumber("09371085854")
                .organization("snappbox")
                .github("alireza-hanifi")
				.build(), Pageable.unpaged());
        Assert.assertEquals("contacts returned from search must be one", 1, ((Collection<?>) contactDtos).size());
    }



}
